import * as vscode from "vscode";
import { LogProvider } from "./logProvider";
import { toLogUri } from "./logUri";
import { WatchEntry } from "../common/config";
import { ConfigService } from "./config";

export const openLogResourceCmd = "logviewer.openLogResource";
const unwatchCmd = "logviewer.unwatchLogResource";
const unwatchAllCmd = "logviewer.unwatchAll";

interface GroupItem {
    readonly kind: "group";
    readonly parent: GroupItem | undefined;
    readonly name: string;
    readonly items: Item[];
}

interface WatchItem {
    readonly kind: "watch";
    readonly parent: GroupItem | undefined;
    readonly title?: string;
    readonly pattern: string;
    readonly uri: vscode.Uri;
}

type Item = GroupItem | WatchItem;

function toItem(entry: WatchEntry, parent: GroupItem | undefined): Item {
    if (entry.kind === "group") {
        const items: Item[] = [];
        const groupItem: GroupItem = {
            kind: "group",
            parent,
            name: entry.groupName,
            items,
        };
        for (const x of entry.watches) {
            items.push(toItem(x, groupItem));
        }
        return groupItem;
    } else {
        return {
            kind: "watch",
            parent,
            pattern: entry.pattern,
            title: entry.title,
            uri: toLogUri(entry)
        };
    }
}

class LogExplorer implements vscode.TreeDataProvider<Item>, vscode.Disposable {

    private readonly disposable: vscode.Disposable;

    public static readonly ViewId: string = "logExplorer";

    private readonly _onDidChange: vscode.EventEmitter<undefined>;

    constructor(
        private readonly logProvider: LogProvider,
        private readonly configSvc: ConfigService
    ) {
        this._onDidChange = new vscode.EventEmitter();

        this.disposable = vscode.Disposable.from(
            this._onDidChange,
            configSvc.onChange(() => {
                this.logProvider.unWatchAll(); // ???
                this._onDidChange.fire(undefined);
            })
        );
    }

    public get onDidChangeTreeData() {
        return this._onDidChange.event;
    }

    public reload() {
        this._onDidChange.fire(undefined);
    }

    public getTreeItem(element: Item) {
        if (element.kind === "group") {
            return new vscode.TreeItem(element.name, vscode.TreeItemCollapsibleState.Expanded);
        } else {
            const watching = this.logProvider.has(element.uri);
            const name = element.title || element.pattern;
            const item = new vscode.TreeItem(name);
            if (watching) {
                item.iconPath = new vscode.ThemeIcon("eye");
                item.contextValue = "watching";
            } else {
                item.iconPath = undefined; //vscode.ThemeIcon.File;
                item.contextValue = undefined;
            }
            item.command = {
                command: openLogResourceCmd,
                arguments: [element.uri.toString()],
                title: name,
                tooltip: name
            };
            return item;
        }
    }

    public getParent(element: Item): GroupItem | undefined {
        return element.parent;
    }

    public getChildren(element?: Item): Item[] | undefined {
        if (element === undefined) {
            // root
            return this.configSvc.getWatches().map(x => toItem(x, undefined));
        } else if (element.kind === "group") {
            return element.items;
        }
    }

    public dispose() {
        this.disposable.dispose();
    }
}

export interface LogExplorerAndTreeView {
    logExplorer: LogExplorer;
    treeView: vscode.TreeView<Item>;
}

export function registerLogExplorer(
    logProvider: LogProvider,
    context: vscode.ExtensionContext,
    configSvc: ConfigService
): void {
    const logExplorer = new LogExplorer(logProvider, configSvc);
    context.subscriptions.push(logExplorer);

    const treeView = vscode.window.createTreeView(LogExplorer.ViewId, {
        treeDataProvider: logExplorer
    });
    context.subscriptions.push(treeView);

    if (global.__logViewer$testing) {
        global.__logViewer$logExplorerAndTreeView = {
            logExplorer,
            treeView
        };
    }

    context.subscriptions.push(
        vscode.commands.registerCommand(openLogResourceCmd, async (logUri) => {
            const uri = vscode.Uri.parse(logUri);
            logProvider.reload(uri);
            const doc = await vscode.workspace.openTextDocument(uri);
            logExplorer.reload();
            await vscode.window.showTextDocument(doc, { preview: false });
        })
    );

    context.subscriptions.push(
        vscode.commands.registerCommand(unwatchCmd, (x) => {
            let uri: vscode.Uri;
            if (typeof x === "string") {
                uri = vscode.Uri.parse(x);
            } else if ((x as { uri: vscode.Uri }).uri instanceof vscode.Uri) {
                uri = (x as { uri: vscode.Uri }).uri;
            } else {
                return;
            }
            logProvider.unWatch(uri);
            logExplorer.reload();
        })
    );

    context.subscriptions.push(
        vscode.commands.registerCommand(unwatchAllCmd, () => {
            logProvider.unWatchAll();
            logExplorer.reload();
        })
    );
}
